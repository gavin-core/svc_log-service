const db = require('../middleware/db')

const _collections = []

const _log = (collection, req, cb) => {
  req.body._date = req.body._date || new Date()
  if (req.body._date) {
    const d = new Date(req.body._date)
    if (d && !isNaN(d.getTime())) {
      req.body._date = d
    }
  }

  let asyncCB = () => {}

  if (req.data.async) {
    asyncCB = cb
    cb = () => {}
  }

  collection.insert(req.body, {safe: !req.data.async}, cb)
  asyncCB(null, { ok: true })
}

const _getCollection = function (req, cb) {
  const key = req.url.substr(1)
  if (!(key in _collections)) {
    _collections[key] = db.getDb().collection(key)
  }

  cb(null, _collections[key])
}

exports.log = (req, res) => {
  _getCollection(req, (err, collection) => {
    if (err) {
      console.log('Error received but not catered for')
      console.error(err)
    }

    _log(collection, req, () => {
      res.sendexpress(null, { ok: true })
    })
  })
}
