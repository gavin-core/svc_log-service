const discovery = require('node-discovery')

const mongo = discovery.mongo
const servicePackage = discovery.servicePackage

let _db
// const _collections = {}

module.exports = {
  configureDbConnections: cb => {
    const dbPool = new mongo.MongoConnectionPool([{
      key: servicePackage.name,
      collections: [],
      connected: db => {
        _db = db
        cb()
      }}
    ])

    dbPool.on('error', cb)
  },
  getDb: () => _db
}
